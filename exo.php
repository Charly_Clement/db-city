<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="exo.css">
    <title>CITY</title>
</head>

<body>

<!--
        NOMBRE DE VILLES TOTAL : 36700

    1) Créer une base de données nommée 'City'
    2) Donner tous les droits à un utilisateur
    3) Importer dans votre base le fichier villes.sql (dispo dans mattermost)

    Une fois votre base remplie, commencer à coder vos premières requêtes SQL préparées
    Votre code doit être en partie sécurisé contre les injections SQL et la faille XSS

    4) Créer un formulaire permettant de saisir un numéro de département (28,72,974,...)
        Le script affichera seulement le nom de la ville et le code postal pour les villes du département concerné, 
        dans l'ordre alphabétique ainsi que le nombre de villes présentes dans le département
        (375 villes doivent ressortir pour le 72)

    5) Créer un formulaire permettant de supprimer toutes les villes,
        sauf celles dont le nombre d'habitants en 2012 est supérieur au chiffre saisi dans votre formulaire
        (200000 supprimera toutes les villes de moins de 200 000 habitants de notre base)
        Afficher le nom des villes restantes et leurs populations en 2012, après la requête de suppression.
        (Il doit rester 11 villes de plus de 200 000 habitants)

    6) Créer un formulaire permettant de modifier le nom d'une ville
        Un champ recevra le nom de la ville actuelle, et un autre champ recevra le nouveau nom de cette ville
        Changer PARIS par CAPITALE
        Afficher le nom des villes restantes

    7) Créer un formulaire permettant d'ajouter une ville.
    
    Nouvelle ville : 

    Département : 99
    Nom : CAMP SAUVAGEON
    CP : 99999
    Pop en 2010 : 5000 habitants
    Pop en 1999 : 300 habitants
    pop en 2012 : 100 habitants
    Taille de leur territoire : 150 km2

    Afficher le département, le nom, la population en 1999, en 2010, en 2012 et la superficie 
    des villes après l'insertion du nouvel enregistrement

    8) Créer un bouton afin qu'il affiche la liste de toutes les villes dans l'ordre des départements.
    Afficher le département, le nom, la population en 1999, en 2010, en 2012 et la superficie des villes

    -->

    <div class="container-fluid">

<!-- SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT..... -->

        <form method="POST">
            <div class="d-flex justify-content-around">
                <input id="remplir" type="text"   name="departement" placeholder="N° du département">
                <input id="valider" type="submit" name="submit1" value="Afficher" class="btn btn-primary">
            </div>
        </form>


        <?php
            $departement  = isset($_POST['departement']) && !empty($_POST['departement']) ? $_POST['departement'] : '';
            $submit1      = isset($_POST['submit1']) ;

            $servername = '127.0.0.1';
            $username   = 'charly_villes';
            $password   = 'LAchartre/LEloir72';
            $db         = 'city';

            $pdo = new PDO("mysql:host=$servername;dbname=$db;", $username, $password);
            $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            if ($submit1) {
                try {
            
                    $req = $pdo->prepare("SELECT ville_nom,ville_code_postal FROM villes WHERE ville_departement=? ORDER BY ville_nom");
                    $req -> execute([$departement]);
                    $results = $req->fetchAll();

                    foreach ($results as $result) {
                        echo $result['ville_nom'] .' ';
                        echo $result['ville_code_postal'] . '<br>';
                    }
                }
                catch(PDOException $e) {
                    echo "Erreur :" . $e->getMessage();
                }
            }

        ?>

<!-- DELETE.....DELETE.....DELETE.....DELETE.....DELETE.....DELETE.....DELETE.....DELETE.....DELETE.....DELETE.....DELETE.....DELETE..... -->

        <form method="POST">
            <div class="d-flex justify-content-around">
                <input id="remplir" type="text"   name="habitant" placeholder="Ville qui ont moins de X habitants">
                <input id="valider" type="submit" name="submit2" value="Supprimer les villes" class="btn btn-primary">
            </div>
        </form>

        <?php
            $habitant = isset($_POST['habitant']) && !empty($_POST['habitant']) ? $_POST['habitant'] : '';
            $submit2  = isset($_POST['submit2']);

            if ($submit2) {
                try {

                    $req = $pdo->prepare("DELETE FROM villes WHERE ville_population_2012<=?");
                    $req -> execute([$habitant]);
                    $req = $pdo->prepare("SELECT ville_nom,ville_population_2012 FROM villes");
                    $req -> execute();
                    $results = $req -> fetchAll();

                    foreach ($results as $result) {
                        
                        echo 
                            '<center>'.$result['ville_nom'] . ' ' . $result['ville_population_2012'] . '<br>'.'</center><br>';
                    }
                    
                }
                catch(PDOException $e) {
                    echo "Erreur :" . $e->getMessage();
                }
            }

        ?>

<!-- UPDATE.....UPDATE.....UPDATE.....UPDATE.....UPDATE.....UPDATE.....UPDATE.....UPDATE.....UPDATE.....UPDATE.....UPDATE.....UPDATE..... -->

        <form method="POST">
            <div class="d-flex justify-content-around">
                <input id="remplir" type="text"   name="ville"          placeholder="Renommé une ville">
                <input id="remplir" type="text"   name="nouvelle_ville" placeholder="Par celle-ci">
                <input id="valider" type="submit" name="submit3" value="Renommer" class="btn btn-primary">
            </div>
        </form>

        <?php
            $ville          = isset($_POST['ville'])          && !empty($_POST['ville'])          ? $_POST['ville'] : '';
            $nouvelle_ville = isset($_POST['nouvelle_ville']) && !empty($_POST['nouvelle_ville']) ? $_POST['nouvelle_ville'] : '';
            $submit3        = isset($_POST['submit3']) ;

            if ($submit3) {
                try {

                    $req = $pdo->prepare("UPDATE villes SET ville_nom = ? WHERE ville_nom = ?");
                    $req -> execute([$nouvelle_ville,$ville]);
                    $req = $pdo->prepare("SELECT * FROM villes");
                    $req -> execute();
                    $results = $req -> fetchAll();

                    foreach ($results as $result) {
                        echo '<center>'.$result['ville_nom'] . '<br>'.'</center>';
                    }
                }
                catch(PDOException $e) {
                    echo "Erreur :" . $e->getMessage();
                }
            }

        ?>

<!-- INSERT.....INSERT.....INSERT.....INSERT.....INSERT.....INSERT.....INSERT.....INSERT.....INSERT.....INSERT.....INSERT.....INSERT..... -->

        <form method="POST">
            <div class="d-flex justify-content-around pt-4">
                <input id="remplir" type="text" name="new_dep"  placeholder="Département">
                <input id="remplir" type="text" name="nom"      placeholder="Nom de la ville">
                <input id="remplir" type="text" name="cp"       placeholder="Code Postal">
                <input id="remplir" type="text" name="pop_2010" placeholder="Population 2010">
            </div>
            
            <div class="d-flex justify-content-around pb-4">
                <input id="remplir" type="text" name="pop_1999" placeholder="Population_1999">
                <input id="remplir" type="text" name="pop_2012" placeholder="Population_2012">
                <input id="remplir" type="text" name="surface"  placeholder="Surface de la ville">
                <input id="valider" type="submit" name="submit4" value="Ajouter" class="btn btn-primary">
            </div>
            
        </form>

        <?php
            $new_dep  = isset($_POST['new_dep'])  && !empty($_POST['new_dep'])  ? $_POST['new_dep']  : '';
            $nom      = isset($_POST['nom'])      && !empty($_POST['nom'])      ? $_POST['nom']      : '';
            $cp       = isset($_POST['cp'])       && !empty($_POST['cp'])       ? $_POST['cp']       : '';
            $pop_2010 = isset($_POST['pop_2010']) && !empty($_POST['pop_2010']) ? $_POST['pop_2010'] : '';
            $pop_1999 = isset($_POST['pop_1999']) && !empty($_POST['pop_1999']) ? $_POST['pop_1999'] : '';
            $pop_2012 = isset($_POST['pop_2012']) && !empty($_POST['pop_2012']) ? $_POST['pop_2012'] : '';
            $surface  = isset($_POST['surface'])  && !empty($_POST['surface'])  ? $_POST['surface']  : '';
            $submit4  = isset($_POST['submit4']);

            if ($submit4) {
        
                try {

                    $req = $pdo->prepare("INSERT INTO villes (ville_departement,ville_nom,ville_code_postal,ville_population_2010,ville_population_1999,
                        ville_population_2012,ville_surface) VALUES (?,?,?,?,?,?,?)");
                    $req -> execute([$new_dep,$nom,$cp, $pop_2010, $pop_1999, $pop_2012, $surface]);
                    
                    $req = $pdo->prepare("SELECT * FROM villes");
                    $req -> execute();
                    $results = $req -> fetchAll();
                    
                    foreach ($results as $result) {
                        echo '<center>'.$result['ville_nom'] . ' ' . $result['ville_population_2012'] . '<br>'.'</center>';
                    }
                }
                catch(PDOException $e) {
                    echo "Erreur :" . $e->getMessage();
                }
            }
            
        ?>

<!-- SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT.....SELECT..... -->

        <form method="POST">
            <div class="text-center">
                <a id="lien" href="?lien=ok" class="btn btn-primary ">Afficher les villes de la base</a>
            </div>
        </form>

        <?php

            $villes = isset($_GET['lien'])  && !empty($_GET['lien'])  ? $_GET['lien']  : '';

            if ($villes == 'ok') {
                $req = $pdo->prepare("SELECT ville_departement,ville_nom,ville_code_postal,ville_population_2010,ville_population_1999,ville_population_2012,ville_surface
                    FROM villes ORDER BY ville_departement ASC");
                $req ->execute();
                $results = $req -> fetchAll();

                echo '<center><table class="w-100 border border-primary mt-3 text-center"><tr><th>Département</th><th>Villes</th><th>Population en 1999</th><th>Population en 2010</th>
                            <th>Population en 2012</th><th>Superficie</th></tr>';

                    foreach ($results as $ville){
                        echo '<tr class="border border-primary"><td>'.$ville['ville_departement'].'</td><td>'.$ville['ville_nom'].'</td><td>'.$ville['ville_population_1999'].
                        '</td><td>'.$ville['ville_population_2010'].'</td><td>'.$ville['ville_population_2012'].'</td><td>'.$ville['ville_surface'].
                        '</td></tr>';
                    }
                    echo '</table></center>';
                }
        ?>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>